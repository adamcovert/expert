$(document).ready(function() {

  var burger = document.querySelector('.burger');
  var navMenu = document.querySelector('.main-nav');

  burger.addEventListener('click', function (event) {

    event.preventDefault();

    if (navMenu.classList.contains('main-nav--is-active')) {
      navMenu.classList.remove('main-nav--is-active');
      return;
    }

    navMenu.classList.add('main-nav--is-active');
  });
});